#STAGE 1: Build
FROM node:16-alpine3.15 AS buildstep
WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
RUN npm run build

# STAGE 2: Run 
FROM nginx:1.21.6-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=buildstep /app/dist/deep-integration /usr/share/nginx/html