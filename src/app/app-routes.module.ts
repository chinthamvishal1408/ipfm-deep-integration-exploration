import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AuthComponent } from "./auth/auth.component";
import { ConsentComponent } from "./consent/consent.component";
import { HomeComponent } from "./home/home.component";


const appRoutes = [
  { path: '', component: AuthComponent},
  { path: 'consent',component: ConsentComponent},
  {path: 'home', component: HomeComponent}
]
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutesModule{
}
