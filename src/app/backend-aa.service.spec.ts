import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Observable, of, throwError } from 'rxjs';

import { BackendAAService } from './backend-aa.service';

describe('BackendAAService Tests', () => {
  // variable declarations
  let service:BackendAAService;
  let httpClientSpyObj : jasmine.SpyObj<HttpClient>;

  beforeEach(() => {

    let spyObj = jasmine.createSpyObj('HttpClient',['post'])

    TestBed.configureTestingModule({
      providers:[
        BackendAAService,
        {provide: HttpClient, useValue: spyObj}
      ]
    });
    service = TestBed.inject(BackendAAService);
    httpClientSpyObj = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return customerId when correct data is sent and request is successfull',()=>{
    const   expectedResponse = {consentHandle : 'randomconsentstring'};
    httpClientSpyObj.post.and.returnValue(of(expectedResponse));
    const data = {customerId: 'string'}
    service.makeConsentRequest(data)
    .subscribe((data)=>{
        expect(data).toEqual(expectedResponse);
    })
  })


  it('should return error when incorrect data is sent',(done: DoneFn)=>{
    const body = {customerId: 'string'}

    const   expectedErrorResponse = new HttpErrorResponse({
      error: 'Invalid body',
      status: 400,

    })

    httpClientSpyObj.post.and.returnValue(throwError(()=> expectedErrorResponse));

    service.makeConsentRequest(body)
    .subscribe({
      next: data => done.fail('expected error but got data'),
      error: error =>{
        expect(error.status).toEqual(400);
        done();
      }
    })
  })

});
