import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendAAService } from '../backend-aa.service';

declare function initSDK(orgId: string, customCSS: {[key:string] : string}) : void
declare function signup(e?: any) : void;
declare function showConsentDetails(e?: any, t?: any) : void
declare function reset() : void;

@Component({
  selector: 'app-consent',
  templateUrl: './consent.component.html',
  styleUrls: ['./consent.component.css']
})
export class ConsentComponent implements OnInit,OnDestroy {
  title = 'deep-integration';
  orgId = 'NON1117'
  customCss = {
    customButtonBackgroundColor: "#f54776",
    customButtonFontColor: "#fff",
    customFontColorHeading: "#079aff",
    customFontColorSubHeading: "#079aff",
    links: "#9c4f4a",
    customBodyBackgroundColor: "#fff"
  }

  constructor(private backendService: BackendAAService, private router: Router) { }

  ngOnInit(): void {
    try{
      initSDK(this.orgId,this.customCss);
    }
    catch(e){
      console.log(e)
    }
    try{
      showConsentDetails(this.backendService.consentHandle)
    }catch(e){
      console.log(e)
    }


  }


  @HostListener('window:OMcallbackTrigger',['$event']) callback(e: any){
    console.log("********************************** EVENT ******************")
      console.log(e)
      if('detail' in e){
        switch(e.detail.data.errorCode){
          case 0:
            // this.backendService.storeArtefact();
            this.router.navigate(['home']);
          default:
            console.log(e.detail)
        }
      }
  }

  ngOnDestroy(): void {
    try {reset();}
    catch(e){
      console.log(e);
    }
  }

}
