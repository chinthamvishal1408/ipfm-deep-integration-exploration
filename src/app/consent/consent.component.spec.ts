import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { BackendAAService } from '../backend-aa.service';

import { ConsentComponent } from './consent.component';

describe('ConsentComponent', () => {
  let component: ConsentComponent;
  let fixture: ComponentFixture<ConsentComponent>;

  beforeEach(async () => {

    const spyObj1 = jasmine.createSpyObj('BackendAAService',null, ['consentHandle']);
    const spyObj2 = jasmine.createSpyObj('Router',['navigate'])
  TestBed.configureTestingModule({
      declarations: [ ConsentComponent ],
      providers:[
        {provide: BackendAAService, useValue: spyObj1},
        {provide: Router, useValue: spyObj2}
      ]
    })
    fixture = TestBed.createComponent(ConsentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const routerSpy = fixture.debugElement.injector.get(Router)
    const backendService = fixture.debugElement.injector.get(BackendAAService)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // MAYBE MOVE THE SDK FUNCTIONS INTO A SERVICE AND TEST THIS COMPONENT!
});
