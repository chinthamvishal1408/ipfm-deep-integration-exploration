import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BackendAAService } from '../backend-aa.service';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {
  consentRequestSubscription : Subscription
  inputError:boolean= false;
  inputForm = this.formBuilder.group({
    mobile: 8106717061
  });

  serverError =false;


  constructor(private formBuilder: FormBuilder, private backendService: BackendAAService, private router: Router) { }

  ngOnInit(): void {
    this.inputForm.valueChanges.subscribe((data:{mobile:string})=>{
     if(data.mobile.match && data.mobile.match('^[0-9]{10}$')){
       this.inputError = false;
     }else{
       this.inputError =true;
      }
      }
    )
  }


  onSubmit(): void {
    //////////////// Make a call to the server and check if the mobile number has a valid consent artifact

    // IF REPSONSE SAYS VALID CONSENT PRESENT THEN ROUTE TO HOME

    // ELSE EXECUTE THE FOLLOWING CODE
    const customerId = this.inputForm.value.mobile+"@onemoney"
    console.log(customerId)
    this.consentRequestSubscription =  this.backendService.makeConsentRequest({customerId})
    .subscribe({
      next: data =>{this.router.navigate(['consent']);},
      error: er =>{console.log(er); this.serverError = true}
    });

  }


  ngOnDestroy(): void {
     if(this.consentRequestSubscription!=null){
      this.consentRequestSubscription.unsubscribe()
     }
  }
}
