import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { utf8Encode } from '@angular/compiler/src/util';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { BackendAAService } from '../backend-aa.service';

import { AuthComponent } from './auth.component';

describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;
  let backendServiceSpy: jasmine.SpyObj<BackendAAService>;
  let routerSpyObj : jasmine.SpyObj<Router>
  beforeEach(() => {
    backendServiceSpy = jasmine.createSpyObj('BackendAAService',['makeConsentRequest']);
    routerSpyObj = jasmine.createSpyObj('Router',['navigate'])
    TestBed.configureTestingModule({
      declarations: [ AuthComponent ],
      providers:[
        {provide: BackendAAService, useValue: backendServiceSpy},
        {provide: Router, useValue: routerSpyObj},
      ],
      imports:[
        ReactiveFormsModule
      ]
    });
  });

  it('should create', () => {

    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('route to consent url when form is submitted and backend return successful response',()=>{
    let  data= {consentHandle : "randomstring"}
    backendServiceSpy.makeConsentRequest.and.returnValue(of(data))

    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    let element = fixture.debugElement

    component.inputForm.patchValue({mobile : 9999999999});

    let button : HTMLButtonElement = element.query(By.css('button')).nativeElement;
    button.click();

    const navArgs = routerSpyObj.navigate.calls.first().args[0];

    expect(navArgs[0])
    .toBe('consent')
  })

  it('set serverError to true when backend server returns an error', ()=>{
    backendServiceSpy.makeConsentRequest.and.returnValue(throwError(()=>{
      return new HttpErrorResponse({
        error: 'Internal Server Errror',
        status: 500
      })
    }))

    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    let element = fixture.debugElement
    component.inputForm.patchValue({mobile : 9999999999});
    let button : HTMLButtonElement = element.query(By.css('button')).nativeElement;

    expect(component.serverError)
    .toBe(false)


    button.click();

    expect(component.serverError)
    .toBe(true)
  })

});

