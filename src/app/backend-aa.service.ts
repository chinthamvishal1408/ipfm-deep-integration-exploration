import { HttpClient } from "@angular/common/http";
import { EventEmitter, Injectable } from "@angular/core";
import { tap} from "rxjs/operators"


@Injectable({providedIn: 'root'})
export class BackendAAService{

  consentHandle : string
  loading = new EventEmitter<boolean>()

  constructor(private http: HttpClient){}

  makeConsentRequest(data: {customerId: string}){
    this.loading.emit(true)
    return this.http.post<{consentHandle: string}>(
      'http://127.0.0.1:3000/aa/consent/request',
      data
    ).pipe(
      tap((response)=>{
        this.consentHandle = response.consentHandle;
        return data;
      })
    );
  }

  // storeArtefact(){
  //   this.loading.emit(true);
  //   return this.http.get<{[key:string]: string}>('http://localhost:80/aa/consent/handle/'+this.consentHandle)
  //   .subscribe(
  //     next=> console.log(next),
  //     e => console.warn(e)
  //   )
  // }
}
